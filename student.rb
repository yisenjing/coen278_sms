require 'dm-core'
require 'dm-migrations'

DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/student_development.db")

class Student
  include DataMapper::Resource
  property :id, Serial
  property :Name, String
  property :Gender, String
  property :Age, Integer
  property :Enrolled_Data, Date
  property :Major, String
  property :Student_id, Integer
  property :Comments, String
  
  def Enrolled_Data=date
    super Date.strptime(date, '%m/%d/%Y')
  end
end

configure do
  enable :sessions
  set :username, 'yuxi'
  set :password, '1234567890'
end

DataMapper.finalize

get '/students' do
  @students = Student.all
  slim :students
end

get '/students/new' do
  halt(401,'Not Authorized') unless session[:admin]
  @student = Student.new
  slim :new_student
end

get '/students/:id' do
  @student = Student.get(params[:id])
  slim :show_student
end

get '/students/:id/edit' do
  halt(401,'Not Authorized') unless session[:admin]
  @student = Student.get(params[:id])
  slim :edit_student
end

post '/students' do  
  student = Student.create(params[:student])
  redirect to("/students/#{student.id}")
end

put '/students/:id' do
  student = Student.get(params[:id])
  student.update(params[:student])
  redirect to("/students/#{student.id}")
end

delete '/students/:id' do
  halt(401,'Not Authorized') unless session[:admin]
  Student.get(params[:id]).destroy
  redirect to('/students')
end