require 'sinatra'
require 'slim'
require 'sass'
require './student'


configure do
  enable :sessions
  set :username, 'yuxi'
  set :password, '1234567890'
end


get('/styles.css'){ scss :styles }

get '/' do
  slim :home
end

get '/about' do
  @title = "All About This Website"
  slim :about
end

get '/students' do
  @title = "All About This Website"
  slim :students
end

get '/contact' do
  slim :contact
end

not_found do
  slim :not_found
end

get '/login' do
  slim :login
end


post '/login' do
  if params[:username] == settings.username && params[:password] == settings.password
    session[:admin] = true
    redirect to('/students')
  else
    slim :login
  end
end

get '/logout' do
  session.clear
  slim :home
end

get '/set/:name' do
  session[:name] = params[:name]
end

get '/get/hello' do
  "Hello #{session[:name]}"
end